package com.skillversum.movieDataBase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

//@ComponentScan("com.skillversum")
@EntityScan("com.skillversum")
@SpringBootApplication(scanBasePackages = "com.skillversum")
public class MovieDataBaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieDataBaseApplication.class, args);
	}
}
