package com.skillversum.repositories;

import com.skillversum.entities.Actor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ActorRepository {

    @PersistenceContext
    private EntityManager em;

    public List<Actor> loadActors(){
        return em.createQuery("select a from Actor a").getResultList();
    }

    public void save(Actor actor){
        if(actor.getId() == null){
            em.persist(actor);
        }
        else{
            em.merge(actor);
        }
    }
    public Actor findById(long id){
        return (Actor) em.createQuery("select a from Actor a where id=:id").setParameter("id",id).getSingleResult();
    }

    public void delete(Actor actor){
        em.remove(actor);
    }

    public Actor findByName(String name){
        return (Actor) em.createQuery("select a from Actor a where name=:name").setParameter("name",name).getSingleResult();
    }
}
