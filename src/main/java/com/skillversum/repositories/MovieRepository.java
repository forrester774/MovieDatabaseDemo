package com.skillversum.repositories;

import com.skillversum.entities.MovieEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class MovieRepository {

    @PersistenceContext
    private EntityManager em;

    public List<MovieEntity> findAll() {
        return em.createQuery("select m from MovieEntity m ").getResultList();
    }
    public MovieEntity findById(long id){
        return (MovieEntity) em.createQuery("select m from MovieEntity m where id=:id").setParameter("id", id)
                .getSingleResult();
    }

    public void save(MovieEntity movieEntity){
        if(movieEntity.getId() == null){
            em.persist(movieEntity);
        }
        else{
            em.merge(movieEntity);
        }

    }

    public void delete(MovieEntity movieEntity){
        em.remove(movieEntity);
    }

}
