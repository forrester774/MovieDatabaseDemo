package com.skillversum.repositories;

import com.skillversum.entities.MovieEntity;
import com.skillversum.entities.TVSeriesEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
@Repository
public class TVSeriesRepository {

    @PersistenceContext
    private EntityManager em;

    public List<TVSeriesEntity> findAll() {
        return em.createQuery("select m from TVSeriesEntity m ").getResultList();
    }
    public TVSeriesEntity findById(long id){
        return (TVSeriesEntity) em.createQuery("select m from TVSeriesEntity m where id=:id").setParameter("id", id)
                .getSingleResult();
    }

    public void save(TVSeriesEntity seriesEntity){
        if(seriesEntity.getId() == null){
            em.persist(seriesEntity);
        }
        else{
            em.merge(seriesEntity);
        }

    }
    public TVSeriesEntity findByTitle(String title){
        return (TVSeriesEntity) em.createQuery("select t from TVSeriesEntity t where title=:title").setParameter("title",title)
                .getSingleResult();
    }

    public void delete(TVSeriesEntity seriesEntity){
        em.remove(seriesEntity);
    }
}
