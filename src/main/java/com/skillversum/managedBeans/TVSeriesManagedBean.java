package com.skillversum.managedBeans;

import com.skillversum.entities.TVSeriesEntity;
import com.skillversum.repositories.TVSeriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Named
@ViewScoped
public class TVSeriesManagedBean {
    private List<TVSeriesEntity> seriesList = new ArrayList<>();
    private TVSeriesEntity selectedSeries;

    @Autowired
    private TVSeriesRepository seriesRepository;

    public TVSeriesManagedBean() {

    }

    @PostConstruct
    public void init() {
        selectedSeries = new TVSeriesEntity();
        loadAll();
    }

    public void loadAll() {
        seriesList = seriesRepository.findAll();
    }

    public void save(TVSeriesEntity seriesEntity) {
        seriesRepository.save(seriesEntity);
        loadAll();
        selectedSeries = new TVSeriesEntity();

    }

    public void delete(long id) {
        selectedSeries = seriesRepository.findById(id);
        seriesRepository.delete(selectedSeries);
        loadAll();
        selectedSeries = new TVSeriesEntity();
    }

    public void modify(long id) {
        selectedSeries = seriesRepository.findById(id);
    }

    public List<TVSeriesEntity> getSeriesList() {
        return seriesList;
    }

    public void deleteButton(ActionEvent actionEvent){
        addMessage("TV Series Deleted!");
    }
    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    public void saveButton(ActionEvent actionEvent){
        addMessage("TV Series Saved!");
    }

    public void setSeriesList(List<TVSeriesEntity> seriesList) {
        this.seriesList = seriesList;
    }

    public TVSeriesEntity getSelectedSeries() {
        return selectedSeries;
    }

    public void setSelectedSeries(TVSeriesEntity selectedSeries) {
        this.selectedSeries = selectedSeries;
    }
}