package com.skillversum.managedBeans;


import com.skillversum.entities.Actor;
import com.skillversum.repositories.ActorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
@Transactional
public class ActorManagedBean {

    private Actor selectedActor;
    private List<Actor> actorList;

    @Autowired
    private ActorRepository actorRepository;

    public ActorManagedBean() {

    }

    @PostConstruct
    public void init(){
        selectedActor = new Actor();
        actorList = new ArrayList<>();
        loadAll();
    }

    private void loadAll(){
        actorList = actorRepository.loadActors();
    }

    public void save(Actor actor){
        actorRepository.save(actor);
        loadAll();
        selectedActor = new Actor();
    }

    public Actor selectActor(long id){
        selectedActor = actorRepository.findById(id);
        return selectedActor;
    }

    public void delete(long id){
        selectedActor = actorRepository.findById(id);
        actorRepository.delete(selectedActor);
        loadAll();
        selectedActor = new Actor();
    }

    public void deleteButton(ActionEvent actionEvent){
        addMessage("Actor Deleted!");
    }
    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    public void saveButton(ActionEvent actionEvent){
        addMessage("Actor Saved!");
    }








    public Actor getSelectedActor() {
        return selectedActor;
    }

    public void setSelectedActor(Actor selectedActor) {
        this.selectedActor = selectedActor;
    }

    public List<Actor> getActorList() {
        return actorList;
    }

    public void setActorList(List<Actor> actorList) {
        this.actorList = actorList;
    }
}
