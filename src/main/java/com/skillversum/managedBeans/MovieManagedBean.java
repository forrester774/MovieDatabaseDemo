package com.skillversum.managedBeans;

import com.skillversum.entities.Actor;
import com.skillversum.entities.MovieEntity;
import com.skillversum.entities.TVSeriesEntity;
import com.skillversum.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Named
@ViewScoped
public class MovieManagedBean {
    private List<MovieEntity> movieList = new ArrayList<>();
    private MovieEntity selectedMovie;
    private List<TVSeriesEntity> tvSeriesEntityList = new ArrayList<>();
    private List<Actor> actorList = new ArrayList<>();

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TVSeriesManagedBean tvSeriesManagedBean;

    @Autowired
    private ActorManagedBean actorManagedBean;

    public MovieManagedBean() {

    }

    @PostConstruct
    public void init() {
        selectedMovie = new MovieEntity();
        loadAll();
        tvSeriesEntityList = tvSeriesManagedBean.getSeriesList();
        actorList = actorManagedBean.getActorList();
    }

    public void loadAll() {
        movieList = movieRepository.findAll();
    }

    public void save(MovieEntity movieEntity) {
        movieRepository.save(movieEntity);
        loadAll();
        selectedMovie = new MovieEntity();

    }

    public void delete(long id) {
        selectedMovie = movieRepository.findById(id);
        movieRepository.delete(selectedMovie);
        loadAll();
        selectedMovie = new MovieEntity();
    }

    public void modify(long id) {
        selectedMovie = movieRepository.findById(id);
    }


    public void deleteButton(ActionEvent actionEvent) {
        addMessage("Movie Deleted!");
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void saveButton(ActionEvent actionEvent) {
        addMessage("Movie Saved!");
    }


    public List<MovieEntity> getMovieList() {
        return movieList;
    }

    public void setMovieList(List<MovieEntity> movieList) {
        this.movieList = movieList;
    }

    public MovieEntity getSelectedMovie() {
        return selectedMovie;
    }

    public void setSelectedMovie(MovieEntity selectedMovie) {
        this.selectedMovie = selectedMovie;
    }

    public List<TVSeriesEntity> getTvSeriesEntityList() {
        return tvSeriesEntityList;
    }

    public void setTvSeriesEntityList(List<TVSeriesEntity> tvSeriesEntityList) {
        this.tvSeriesEntityList = tvSeriesEntityList;
    }

    public List<Actor> getActorList() {
        return actorList;
    }

    public void setActorList(List<Actor> actorList) {
        this.actorList = actorList;
    }
}
