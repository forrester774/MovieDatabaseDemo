package com.skillversum.converter;

import com.skillversum.entities.TVSeriesEntity;
import com.skillversum.repositories.TVSeriesRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;

@Named
@ApplicationScoped
public class SeriesConverter implements Converter {
    @Autowired
    private TVSeriesRepository tvSeriesRepository;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if (s == null) {
            return null;
        }
        return tvSeriesRepository.findByTitle(s);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (o == null) {
            return "";
        }
        return ((TVSeriesEntity) o).getTitle();
    }
}
