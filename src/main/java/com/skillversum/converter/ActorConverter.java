package com.skillversum.converter;

import com.skillversum.entities.Actor;
import com.skillversum.repositories.ActorRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;


@Named
@ApplicationScoped
public class ActorConverter implements Converter {

    @Autowired
    private ActorRepository actorRepository;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if (s == null) {
            return null;
        }
        return actorRepository.findByName(s);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (o == null) {
            return "";
        }
        return ((Actor) o).getName();
    }
}
