package com.skillversum.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "tv_series")
public class TVSeriesEntity extends AbstractAnimation implements Serializable {

    @Column
    private Integer seasons;

    public TVSeriesEntity() {

    }

    public TVSeriesEntity(String title, Date releaseDate, Integer rating, Integer seasons) {
        super(title, releaseDate, rating);
        this.seasons = seasons;
    }

    public Integer getSeasons() {
        return seasons;
    }

    public void setSeasons(Integer seasons) {
        this.seasons = seasons;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        return this.getTitle().equals(((TVSeriesEntity) obj).getTitle());
    }
}
